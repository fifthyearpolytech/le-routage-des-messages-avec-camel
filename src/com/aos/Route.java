package com.aos;

import java.io.File;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFileMessage;
import org.apache.camel.impl.DefaultCamelContext;

public class Route {

	public static void main(String[] args) {
		try {
			CamelContext context = new DefaultCamelContext();
			context.addRoutes(new RouteBuilder() {
				public void configure() throws Exception {
					from("file://in").process(new Processor() {
						public void process(Exchange e) throws Exception {
							GenericFileMessage<File> fileIn = (GenericFileMessage<File>) e.getIn();
							
							String contain = fileIn.getBody(String.class);
							System.out.println("Echange reçu : " + fileIn);
							System.out.println(contain);
							//
							// COMPLETER AVEC L’AIDE DE LA JAVADOC CAMEL
							//
						}
					}).to("file://out");
				}
			});
			context.start();
			// ajouter ici un code permettant de pauser le thread courant
			while(true) {
				
			}
			//context.stop();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
}
